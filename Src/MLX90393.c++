/*
 * MLX90393.c++
 *
 *  Created on: May 3, 2022
 *      Author: lukaswuttke
 */

#include "MLX90393.h"

// Date constructor
MLX90393::MLX90393(I2C_HandleTypeDef *hi2c1, uint8_t MLX90393_ADDR) {
	hi2c = hi2c1;
	address_i2c = MLX90393_ADDR;
	init();
}

// Date member function
void MLX90393::init() {

	HAL_Delay(20);

	reset();

	HAL_Delay(20);

}

//Warm reset the IC
bool MLX90393::reset() {

	send_buf[0] = 0x80;

	//Send Command for Exit
	if (transceive(1, 1) == false) {
		return false;
	}

	send_buf[0] = 0xF0;

	//Send Command for Reset
	if (transceive(1, 1) == false) {
		return false;
	}

	//Check if IC has been warm reset
	if ((this->status & 0x04) == 0x04) {
		return true;
	} else {
		return false;
	}

}

bool MLX90393::startBurstMode(bool x, bool y, bool z, bool t) {

	send_buf[0] = 0x10 | (t << 0) | (x << 1) | (y << 2) | (z << 3);
//Send Command
	if (transceive(1, 1) == false) {
		return false;
	}

//Check if Burst Mode is started
	if ((this->status & 0x80) == 0x80) {
		this->modus = 1;
		return true;
	} else {
		this->modus = 0;
		return false;
	}
}

bool MLX90393::startSingleMeasurementMode(bool x, bool y, bool z, bool t) {

	send_buf[0] = 0x30 | (t << 0) | (x << 1) | (y << 2) | (z << 3);
	//Send Command
	if (transceive(1, 1) == false) {
		return false;
	}

	//Check if Single Measurement Mode is started
	if ((this->status & 0x20) == 0x20) {
		this->modus = 3;
		return true;
	} else {
		this->modus = 0;
		return false;
	}
}

bool MLX90393::readMeasurement(bool x, bool y, bool z, bool t) {

	startSingleMeasurementMode(x,y,z,t);

	memset (rec_buf, 0, sizeof(rec_buf));
	uint8_t rec_length = (uint8_t(x) + uint8_t(y) + uint8_t(z) + uint8_t(t))*2;
	send_buf[0] = 0x40 | (t << 0) | (x << 1) | (y << 2) | (z << 3);
	//Send Command
	if (transceive(1, rec_length, 20) == false) {
		return false;
	}
	else{
		t_value = (rec_buf[1] << 8) | rec_buf[2];
		x_value = (rec_buf[3] << 8) | rec_buf[4];
		y_value = (rec_buf[5] << 8) | rec_buf[6];
		z_value = (rec_buf[7] << 8) | rec_buf[8];
		return true;
	}

	//Check if Single Measurement Mode is started
	//if ((this->status | 0x20) == true) {
	//	this->modus = 3;
	//	return true;
	//} else {
	//	this->modus = 0;
	//	return false;
	//}
}


bool MLX90393::readRegister(uint8_t register_addr){


	//memset (rec_buf, 0, sizeof(rec_buf));
	//Command for Read Register
	send_buf[0] = 0x50;
	//Set Register Address
	send_buf[1] = register_addr << 2;
	//Send Command
	if (transceive(2, 2, 2) == false) {
		return false;
	}
	else{
		return true;
	}

	//Check if Single Measurement Mode is started
	//if ((this->status | 0x20) == true) {
	//	this->modus = 3;
	//	return true;
	//} else {
	//	this->modus = 0;
	//	return false;
	//}
}


bool MLX90393::writeRegister(uint8_t register_addr){


	//memset (rec_buf, 0, sizeof(rec_buf));

	readRegister(register_addr);

	HAL_Delay(2);

	send_buf[1] = rec_buf[1];
	send_buf[2] = rec_buf[2];

	send_buf[2] = 0b01111100;

	//Command to Write Register
	send_buf[0] = 0x60;

	//Set Register Address
	send_buf[3] = register_addr << 2;
	//Send Command
	if (transceive(4, 1, 1) == false) {
		return false;
	}
	else{
		return true;
	}

	//Check if Single Measurement Mode is started
	//if ((this->status | 0x20) == true) {
	//	this->modus = 3;
	//	return true;
	//} else {
	//	this->modus = 0;
	//	return false;
	//}
}


/**
 * Performs a full read/write transaction with the sensor.
 *
 * @param txlen     The number of bytes to write.
 * @param rxlen     The number of bytes to read back (not including the
 *                  mandatory status byte that is always returned).
 *
 */

bool MLX90393::transceive(uint8_t txlen, uint8_t rxlen, uint8_t interdelay) {

	ret = HAL_I2C_Master_Transmit(hi2c, address_i2c, send_buf, txlen,
	HAL_MAX_DELAY);
	if (ret != HAL_OK) {
		status = MLX90393_STATUS_ERROR;
		return false;
	} else if (rxlen > 0) {
		if (rxlen > 1) rxlen++;
		HAL_Delay(interdelay);
		ret = HAL_I2C_Master_Receive(hi2c, address_i2c, rec_buf, rxlen,
		HAL_MAX_DELAY);

		if (ret != HAL_OK) {
			status = MLX90393_STATUS_ERROR;
			return false;
		} else {
			status = rec_buf[0];
			return true;
		}
	} else {
		return true;
	}
}
