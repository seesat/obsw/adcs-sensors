/*
 * MLX90393.h
 *
 *  Created on: May 3, 2022
 *      Author: lukaswuttke
 */

#ifndef INC_MLX90393_H_
#define INC_MLX90393_H_

#include "stm32f4xx_hal.h"
#include <cstring>

#define MLX90393_DEFAULT_ADDR (0x0C) /* Can also be 0x18, depending on IC */

#define MLX90393_AXIS_ALL (0x0E)      /**< X+Y+Z axis bits for commands. */
#define MLX90393_CONF1 (0x00)         /**< Gain */
#define MLX90393_CONF2 (0x01)         /**< Burst, comm mode */
#define MLX90393_CONF3 (0x02)         /**< Oversampling, filter, res. */
#define MLX90393_CONF4 (0x03)         /**< Sensitivty drift. */
#define MLX90393_GAIN_SHIFT (4)       /**< Left-shift for gain bits. */
#define MLX90393_HALL_CONF (0x0C)     /**< Hall plate spinning rate adj. */
#define MLX90393_STATUS_OK (0x00)     /**< OK value for status response. */
#define MLX90393_STATUS_SMMODE (0x08) /**< SM Mode status response. */
#define MLX90393_STATUS_RESET (0x01)  /**< Reset value for status response. */
#define MLX90393_STATUS_ERROR (0xFF)  /**< ERROR value for status response. */
#define MLX90393_STATUS_MASK (0xFC)   /**< Mask for status OK checks. */

class MLX90393 {
private:

	HAL_StatusTypeDef ret;
	I2C_HandleTypeDef *hi2c;

public:
	uint8_t modus = 0; //0=UNDEFINED, 1=BURST_MODE, 2=WOC_MODE, 3=SM_MODE
	uint8_t send_buf[6] = {0};
	uint8_t rec_buf[12]= {0};
	uint8_t status;
	uint8_t address_i2c;

	uint16_t x_value = 0;
	uint16_t y_value = 0;
	uint16_t z_value = 0;
	uint16_t t_value = 0;

	bool send_error = 0;

	bool receive_error = 0;


	MLX90393(I2C_HandleTypeDef *hi2c1, uint8_t MLX90393_ADDR =
			MLX90393_DEFAULT_ADDR);

	void init();

	bool startBurstMode(bool x = 0, bool y = 0, bool z = 0, bool t = 0);

	bool startWakeOnChangeMode();

	bool startSingleMeasurementMode(bool x = 0, bool y = 0, bool z = 0, bool t =
			0);

	bool readMeasurement(bool x = 0, bool y = 0, bool z = 0, bool t = 0);

	bool readRegister(uint8_t register_addr);

	bool writeRegister(uint8_t register_addr);

	void exitMode();

	void memoryRecall();

	void memoryStore();

	void reset_i2c();

	bool reset();

	bool transceive(uint8_t txlen, uint8_t rxlen = 0, uint8_t interdelay = 10);

};

#endif /* INC_MLX90393_H_ */
